import React, {useContext, useEffect} from "react"
import '../App.css';
import {MovieListProvider} from "../component/MovieEditorContext"
import MovieListForm from "../component/MovieListForm"

const Movie = () =>{
  return(
    <>
      <MovieListProvider>
        <MovieListForm/>
      </MovieListProvider>
    </>
  )
}

export default Movie
