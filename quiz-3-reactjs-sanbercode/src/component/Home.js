import React from "react";
import "../App.css";
import { MovieListProvider } from "./MovieEditorContext";

import HomeContent from "./HomeContent";

const TesHome = () => {
  return (
    <>
      <MovieListProvider>
        <HomeContent />
      </MovieListProvider>
    </>
  );
};

export default TesHome;
