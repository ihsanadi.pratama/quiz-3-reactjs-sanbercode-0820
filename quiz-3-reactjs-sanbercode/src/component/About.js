import React from "react";
import "../App.css";

const About = () => {
  return (
    <div>
      
      <section class="content">
        <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
        <ol>
          <li>
            <span class="bold">Nama:</span> Ihsan Adi Pratama
          </li>
          <li>
            <span class="bold">Email:</span> ihsanadi.pratama@yahoo.com
          </li>
          <li>
            <span class="bold">Sistem Operasi yang digunakan:</span> Microsoft
            Windows
          </li>
          <li>
            <span class="bold">Akun Gitlab:</span> ihsanadi.pratama
          </li>
          <li>
            <span class="bold">Akun Telegram:</span> DrMarvin
          </li>
        </ol>
      </section>
      <footer>
        <h5 class="copyTxt">copyright &copy; 2020 by Sanbercode</h5>
      </footer>
    </div>
  );
};

export default About;
