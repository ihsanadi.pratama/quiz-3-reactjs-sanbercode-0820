import React, { useContext, useEffect, useState } from "react";
import "../App.css";
import axios from "axios";
import { MovieListContext } from "./MovieEditorContext";

const HomeContent = () => {
  const [movieList, setMovieList] = useContext(MovieListContext);
  const [input, setInput] = useState({
    title: "",
    description: "",
    year: 2020,
    duration: 120,
    genre: "",
    rating: 0,
    image_url: "",
  });

  useEffect(() => {
    if (movieList.lists === null) {
      axios
        .get(`http://backendexample.sanbercloud.com/api/movies`)
        .then((res) => {
          setMovieList({
            ...movieList,
            lists: res.data.map((el) => {
              return {
                id: el.id,
                title: el.title,
                description: el.description,
                year: el.year,
                duration: el.duration,
                genre: el.genre,
                rating: el.rating,
                image_url: el.image_url,
              };
            }),
          });
        });
    } else if (movieList.statusForm === "changeToEdit") {
      let movieData = movieList.lists.find(
        (x) => x.id === movieList.selectedId
      );
      setInput({
        title: movieData.title,
        description: movieData.description,
        year: movieData.year,
        duration: movieData.duration,
        genre: movieData.genre,
        rating: movieData.rating,
        image_url: movieData.image_url,
      });
      setMovieList({ ...movieList, statusForm: "edit" });
    }
  }, [movieList, setMovieList]);

  return (
    <div>
      <section class="content">
        <h1>Daftar Film Film Terbaik</h1>

        {movieList.lists !== null &&
          movieList.lists.map((item, index) => {
            return (
              <>
                <div>
                  <p className="boldTxt">{item.title}</p>
                  <img src={item.image_url} id="movieStyle" />
                  <p className="boldTxt">Rating{item.duration}</p>
                  <p className="boldTxt">Durasi: {item.genre}</p>
                  <p className="boldTxt">genre: {item.rating}</p>
                  <p>
                    <span className="boldTxt">deskripsi</span>
                    {item.description}
                  </p>
                </div>
                <div className="separator"></div>
              </>
            );
          })}
      </section>
      <footer>
        <h5 class="copyTxt">copyright &copy; 2020 by Sanbercode</h5>
      </footer>
    </div>
  );
};

export default HomeContent;
