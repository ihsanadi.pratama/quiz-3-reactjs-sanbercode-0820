import React, {useContext, useState, useEffect} from "react"
import {MovieListContext} from "./MovieEditorContext"



const Login = () =>  {
    const [movieList, setMovieList] = useContext(MovieListContext)
    const [input, setInput] = useState({name: "", password: ""})

    const handleChange = (event) =>{
        let typeOfInput = event.target.name
    
        switch (typeOfInput){
          case "name":
          {
            setInput({...input, name: event.target.value});
            break
          }
          case "password":
          {
            setInput({...input, password: event.target.value});
            break
          }
        default:
          {break;}
        }
      }
    
      const handleSubmit = (event) =>{


    event.preventDefault();
    setMovieList({isLogin:true})
       window.location.href = "/";

    //    setMovieList({isLogin:true})

   
    
      }
    

  return (
    <div className="HomeBody">
      <section class="content">
      <form onSubmit={handleSubmit} > {/* onSubmit={handleSubmit} */}
            <label style={{float: "left"}}>
              Username:
            </label>
            <input  type="text" name="name" value={input.name} onChange={handleChange}/>
            <br/>
            <br/>
            <label >
              Password:
            </label>
            <input  type="password" name="password" value={input.price} onChange={handleChange}/>
            <br/>
            <br/>
            <div style={{width: "100%", paddingBottom: "20px"}}>
              <button >Login</button>
            </div>
          </form>

      </section>
    </div>
  );
}

export default Login;
