import React, { useContext } from "react";
import { Link } from "react-router-dom";
import Logo from "../img/logo.png";
import "../App.css";
import {MovieListContext} from "./MovieEditorContext"

const Nav = () => {
    const [movieList, setMovieList] = useContext(MovieListContext)
  return (
    <header class="mainHeader">
      <img id="logo" src={Logo} width="200px" />
      <nav class="titleNav">
        <ul>
          <li className="navBar">
            <Link to="/">Home</Link>
          </li>
          <li className="navBar">
            <Link to="/about">About</Link>
          </li>
          {movieList.isLogin ? <div></div> :
          <li className="navBar">
            <Link to="/movies">Movie List Editor</Link>
          </li> 
          
          }
          <li className="navBar">
            <Link to="/login">Login</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
};

export default Nav;
