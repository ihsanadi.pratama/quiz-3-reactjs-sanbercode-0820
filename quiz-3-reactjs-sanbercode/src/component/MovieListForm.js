import React, {useContext, useEffect, useState} from "react"
import axios from "axios"
import '../App.css';
import {MovieListContext} from "./MovieEditorContext"

const MovieListForm = () =>  {
    const [movieList, setMovieList] = useContext(MovieListContext)
    const [input, setInput] = useState({title: "", 
      description: "",
      year: 2020, 
      duration: 120, 
      genre: "", 
      rating: 0,
      image_url: ""
    })

    useEffect( () => {
        if (movieList.lists === null){
          axios.get(`http://backendexample.sanbercloud.com/api/movies`)
          .then(res => {
            setMovieList({
              ...movieList, 
              lists: res.data.map(el=>{ 
                return {id: el.id,
                  title: el.title, 
                  description: el.description,
                  year: el.year, 
                  duration: el.duration, 
                  genre: el.genre, 
                  rating: el.rating,
                  image_url: el.image_url 
                }
              })
            })
          })
        }else if(movieList.statusForm === "changeToEdit"){
          let movieData = movieList.lists.find(x=> x.id === movieList.selectedId)
      setInput({title: movieData.title, description: movieData.description, year: movieData.year, duration: movieData.duration, genre: movieData.genre, rating: movieData.rating, image_url: movieData.image_url})
      setMovieList({...movieList, statusForm: "edit"})

        }
      }, [movieList, setMovieList])

      const handleChange = (event) =>{
        let typeOfInput = event.target.name
    
        switch (typeOfInput){
          case "title":
          {
            setInput({...input, title: event.target.value});
            break
          }
          case "description":
          {
            setInput({...input, description: event.target.value});
            break
          }
          case "year":
          {
            setInput({...input, year: event.target.value});
              break
          }
          case "duration":
          {
            setInput({...input, duration: event.target.value});
              break
          }
          case "genre":
          {
            setInput({...input, genre: event.target.value});
              break
          }
          case "rating":
          {
            setInput({...input, rating: event.target.value});
              break
          }
          case "image_url":
          {
            setInput({...input, image_url: event.target.value});
              break
          }

        default:
          {break;}
        }
      }

      const handleSubmit = (event) =>{
        event.preventDefault()
    
        let title = input.title
        let description = input.description
        let year = input.year.toString()
        let duration = input.duration.toString()
        let genre = input.genre
        let rating = input.rating.toString()
        let image_url = input.image_url
        
    
        if (title.replace(/\s/g,'') !== "" && description.replace(/\s/g,'') !== "" && year.replace(/\s/g,'') !== "" && duration.replace(/\s/g,'') !== "" && genre.replace(/\s/g,'') !== "" && rating.replace(/\s/g,'') !== ""){      
          if (movieList.statusForm === "create"){        
            axios.post(`http://backendexample.sanbercloud.com/api/movies`, {title: input.title, description: input.description, year: input.year, duration: input.duration, genre: input.genre, rating: input.rating, image_url: input.image_url})
            .then(res => {
              setMovieList(
                  {statusForm: "create", selectedId: 0,
                  lists: [
                    ...movieList.lists, 
                    { id: res.data.id, 
                      title: input.title, 
                      description: input.description,
                      year: input.year,
                      duration: input.duration, 
                      genre: input.genre,
                      rating: input.rating,
                      image_url: input.image_url
                    }]
                  })
            })
          }else if(movieList.statusForm === "edit"){
            axios.put(`http://backendexample.sanbercloud.com/api/movies/${movieList.selectedId}`, {title: input.title, description: input.description, year: input.year, duration: input.duration, genre: input.genre, rating: input.rating})
            .then(() => {
                let movieData = movieData.lists.find(el=> el.id === movieData.selectedId)
                movieData.name = input.title
                movieData.price = input.description
                movieData.weight = input.year
                movieData.duration = input.duration
                movieData.genre = input.genre
                movieData.rating = input.rating
                movieData.image_url = input.image_url
                setMovieList({statusForm: "create", selectedId: 0, lists: [...movieList.lists]})
            })
          }
    
          setInput({title: "", 
          description: "",
          year: 2020, 
          duration: 120, 
          genre: "", 
          rating: 0})
        }
      }
    
    
    

      const handleEdit = (event) =>{
        let idmovieList = parseInt(event.target.value)
        setMovieList({...movieList, selectedId: idmovieList, statusForm: "changeToEdit"})
      }

      const handleDelete = (event) => {
        let idmovieList = parseInt(event.target.value)
    
        let newLists = movieList.lists.filter(el => el.id !== idmovieList)
    
        axios.delete(`http://backendexample.sanbercloud.com/api/movies/${idmovieList}`)
        .then(res => {
          console.log(res)
        })
              
        setMovieList({...movieList, lists: [...newLists]})
        
      }
    
    

      
  return (
      <div>
    <section class="content">
    
        <form>
      <input type="search"/>
      <button>search</button>
      </form>

      <h1>Daftar Film</h1>
      <table>
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th>Year</th>
            <th>Duration</th>
            <th>Genre</th>
            <th>Rating</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>

            {
              movieList.lists !== null && movieList.lists.map((item, index)=>{
                return(    
                  <tr key={index}>
                    <td>{index+1}</td>
                    <td>{item.title}</td>
                    <td>{item.description}</td>
                    <td>{item.year}</td>
                    <td>{item.duration}</td>
                    <td>{item.genre}</td>
                    <td>{item.rating}</td>
                    <td>
                      <button onClick={handleEdit} value={item.id}>Edit</button>
                      &nbsp;
                      <button onClick={handleDelete} value={item.id}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
        </tbody>
      </table>    

      <h1>Movies Form</h1>

<div style={{width: "50%", margin: "0 auto", display: "block"}}>
  <div style={{border: "1px solid #aaa", padding: "20px"}}>
    <form onSubmit={handleSubmit}>
      <label style={{float: "left"}}>
        Title:
      </label>
      <input style={{float: "right"}} type="text" name="title" value={input.title} onChange={handleChange}/>
      <br/>
      <br/>
      <label style={{float: "left"}}>
        Description:
      </label>
      <input style={{float: "right"}} type="text" name="description" value={input.description} onChange={handleChange}/>
      <br/>
      <br/>
      <label style={{float: "left"}}>
        Year:
      </label>
      <input style={{float: "right"}} type="number" name="year" value={input.year} onChange={handleChange} min="1980"/>
      <br/>
      <br/>
      <label style={{float: "left"}}>
        Duration:
      </label>
      <input style={{float: "right"}} type="number" name="duration" value={input.duration} onChange={handleChange}/>
      <br/>
      <br/>
      <label style={{float: "left"}}>
        Genre:
      </label>
      <input style={{float: "right"}} type="text" name="genre" value={input.genre} onChange={handleChange}/>
      <br/>
      <br/>
      <label style={{float: "left"}}>
        Rating:
      </label>
      <input style={{float: "right"}} type="number" name="rating" value={input.rating} onChange={handleChange} min="0" max="10"/>
      <br/>
      <br/>
      <label style={{float: "left"}}>
        Image Url:
      </label>
      <input style={{float: "right"}} type="text" name="image_url" value={input.image_url} onChange={handleChange}/>
      <br/>
      <br/>
      <div style={{width: "100%", paddingBottom: "20px"}}>
        <button style={{ float: "right"}}>submit</button>
      </div>
    </form>
  </div>
</div>



        
    </section>
      <footer>
        <h5 class="copyTxt">copyright &copy; 2020 by Sanbercode</h5>
      </footer>
    </div>
  );
  
}

export default MovieListForm