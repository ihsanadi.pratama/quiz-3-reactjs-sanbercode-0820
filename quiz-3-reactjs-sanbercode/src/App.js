import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Home from "./component/Home";
import About from "../src/component/About";
import Login from "../src/component/Login";
import Movie from "./component/Movie";
import Nav from "../src/component/Nav";
import {MovieListProvider} from "../src/component/MovieEditorContext"

function App() {
  return (
    <div className="HomeBody">
      <BrowserRouter>
      <MovieListProvider>
        <Nav />
        </MovieListProvider>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>

          <Route exact path="/about">
            <About />
          </Route>

          <Route exact path="/movies">
            <Movie />
          </Route>

          <Route exact path="/login">
          <MovieListProvider>
            <Login />
            </MovieListProvider>
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
